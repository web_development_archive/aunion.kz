<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title> Рекламное агенство </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <!--<link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />-->
    <link rel="stylesheet" href="css/styles.css" />
    <link rel="stylesheet" href="css/lightbox.css" />
    <link rel="stylesheet" type="text/css" href="css/hover-min.css">
    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/lightbox.js"></script>
    <script type="text/javascript" src="js/slides.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
</head>
<body>
<img src="img/кнопка01.png" class="phone_ring ">
<div class="overlay" title="окно"></div>
<div class="popup">
    <div class="close_window">x</div>
    <p class="win_t">Введите ваше имя и телефон и мы с вами свяжемся!</p>
    <form action="call.php" method="post">
        <input type="text" name="call_name" placeholder="Ваше имя" id="call_name" class="call_t">
        <input type="text" name="call_phone" placeholder="Ваш телефон" id="call_phone" class="call_t">
        <input type="button" value="Жду звонка!"  id="submit_call" class="cnt_3_2_1_btn" name="send">
    </form>
</div>

<div class="c_1">
    <div class="c_1_1">
        <img src="img/лого.png" class="logo_1" alt="">
    </div>
    <div class="c_1_2">
        Оставьте заявку
    </div>
    <div class="c_1_3">
        Мы гарантируем конфиденциальность ваших данных
    </div>
</div>
<form id="contactform1">
    <div class="c_2">
        <div class="c_2_1">
            <div class="c_2_1_i">
                <img src="img/чел.png" alt="">
                <input type="text" class="inp_1" name="name" value="" placeholder="Ваше имя" >
            </div>
            <div class="c_2_1_i">
                <img src="img/пис.png" alt="">
                <input type="text" class="inp_1" name="email" value="" placeholder="Ваш E-mail" >
            </div>
            <div class="c_2_1_i">
                <img src="img/тел.png" alt="">
                <input type="text" class="inp_1" name="phone" value="" placeholder="Ваш номер телефона" >
            </div>
            <img src="img/звонок.png" alt="" class="c_2_i" id="zakaz_1">
        </div>
        <div class="c_2_2">
            Мы свяжемся в ближайшее время
        </div>
    </div>
</form>
<div class="c_3">
    г. Алматы, пр. Аль-Фараби 71/21, тел.: +7 /727/ 221 31 13
</div>
<div class="c_4">
    <div class="c_4_1">
        О компании
    </div>
    <div class="c_4_2">
        Рекламное агенство «Union Media» предоставляет своим клиентам полный спектр услуг в области рекламы и продвижения. Благодаря четко согласованной системе, каждый этап работы здесь становится частью единого творческого механизма, обеспечивающего качественное и оперативное решение любой рекламной задачи за счёт отсутствия пауз, которые требуются компаниям-посредникам для согласования действий подрядных организаций. «Union Media»- это уникальное рекламное агентство Алматы, которое предлагает следующие услуги.
    </div>
</div>
<div class="c_5">
    <div class="c_5_1">Наши услуги</div>
    <div class="c_5_2">
        <div class="c_5_2_u">
            <img src="img/нба.png" alt="" class="hvr-bob c_5_2_i">
            <div class="c_5_2_t">Наружное брендирование<br />автотранспорта</div>
            <div class="c_5_2_c">Полное брендирование<br />автобусов, поездов, такси Колеса</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/рва.png" alt="" class="hvr-bob c_5_2_i">
            <div class="c_5_2_t">Реклама в автобусе</div>
            <div class="c_5_2_c">Изготовление брендированной<br />ручки, на сиденьях</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/рвп.png" alt="" class="hvr-bob c_5_2_i">
            <div class="c_5_2_t">Реклама в поездах</div>
            <div class="c_5_2_c">Брошюры, реклама на билетах, <br />мониторы, журналы, газеты</div>
        </div>
        <br />
        <div class="c_5_2_u">
            <img src="img/нр.png" alt="" class="hvr-bob c_5_2_i">
            <div class="c_5_2_t">Наружная реклама</div>
            <div class="c_5_2_c">Билборды, лайтбоксы</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/рвт.png" alt="" class="hvr-bob c_5_2_i">
            <div class="c_5_2_t">Реклама в такси</div>
            <div class="c_5_2_c">Брошюры, рекламный щит,<br />буклеты, брендирование на машину</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/пп.png" alt="" class="hvr-bob c_5_2_i">
            <div class="c_5_2_t">Полиграфическая продукция</div>
            <div class="c_5_2_c">Визитки, брошюры, буклеты,<br />афишы, постеры, флаера и т.д. </div>
        </div>
    </div>
</div>
<div class="c_6">
    <div class="c_6_1">С нами вы можете разместить рекламу <br />на любом транспорте по всей Республики</div>
    <div class="c_6_2">
        <div class="c_6_2_1">
            <div class="c_6_2_n" id="num_1">0</div>     
            <div class="c_6_2_c">Автобусов по Казахстану</div>
        </div>
        <div class="c_6_2_1">
            <div class="c_6_2_n" id="num_2">0</div>     
            <div class="c_6_2_c">Маршрутов поездов по Казахстану и СНГ</div>
        </div>
        <div class="c_6_2_1">
            <div class="c_6_2_n" id="num_3">0</div>
            <div class="c_6_2_c">Регионов в РК</div>       
        </div>
        <div class="c_6_2_1">
            <div class="c_6_2_n" id="num_4">0</div>
            <div class="c_6_2_c">Машин такси</div>
        </div>        
    </div>
</div>

<div class="c_7">
    <div class="c_7_1">Причины по которым с нами работают<br />и нас рекомендуют</div>
    <div class="c_7_2">
        <div class="c_5_2_u">
            <img src="img/ДР.png" alt="" class="hvr-wobble-horizontal c_5_2_i">
            <div class="c_5_2_t">Держим сроки</div>
            <div class="c_5_2_c">Мы всегда держим<br />сроки изготовления</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/ВК.png" alt="" class="hvr-wobble-horizontal c_5_2_i">
            <div class="c_5_2_t">Высокое качество </div>
            <div class="c_5_2_c">Изготовление дизайна<br />и рекламы в высоком качестве</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/НГ.png" alt="" class="hvr-wobble-horizontal c_5_2_i">
            <div class="c_5_2_t">Наши гарантии</div>
            <div class="c_5_2_c">Даем гарантии на все <br />что изготовляем</div>
        </div><br />
        <div class="c_5_2_u">
            <img src="img/М.png" alt="" class="hvr-wobble-horizontal c_5_2_i">
            <div class="c_5_2_t">Материалы</div>
            <div class="c_5_2_c">У нас всегда есть<br />материалы в наличии</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/ЛП.png" alt="" class="hvr-wobble-horizontal c_5_2_i">
            <div class="c_5_2_t">Лояльный подход</div>
            <div class="c_5_2_c">Мы предоставляем свои идеи <br />для вашей рекламы</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/С.png" alt="" class="hvr-wobble-horizontal c_5_2_i">
            <div class="c_5_2_t">Сопровождение</div>
            <div class="c_5_2_c">Креативное сопровождение<br />а все время нашего сотрудничества</div>
        </div>

    </div>
</div>

<div class="c_4">
    <div class="c_4_1">
        Нам доверяют
    </div>
    <div class="c_4_2">
        <img src="img/spons/01.png" class="spons" alt="">
        <img src="img/spons/03.png" class="spons" alt="">
        <img src="img/spons/04.png" class="spons" alt="">
        <img src="img/spons/05.png" class="spons" alt="">
        <img src="img/spons/06.png" class="spons" alt="">
        <img src="img/spons/07.png" class="spons" alt="">
        <img src="img/spons/08.png" class="spons2" alt="">
        <img src="img/spons/09.png" class="spons2" alt="">
        <img src="img/spons/10.png" class="spons2" alt="">
        <img src="img/spons/11.png" class="spons2" alt="">
        <img src="img/spons/12.png" class="spons2" alt="">
        <img src="img/spons/13.png" class="spons2" alt="">
        <img src="img/spons/14.png" class="spons2" alt="">
        <img src="img/spons/15.png" class="spons2" alt="">
        <img src="img/spons/16.png" class="spons2" alt="">
    </div>
</div>

<div class="c_7">
    <div class="c_7_1">Как мы работаем</div>
    <div class="c_7_2">
        <div class="c_5_2_u">
            <img src="img/вознс.png" alt="" class="hvr-bounce-out c_7_2_i">
            <div class="c_5_2_c">Вы оставляете<br />заявку на сайте</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/свснмппиоивд.png" alt="" class="hvr-bounce-out c_7_2_i">
            <div class="c_5_2_c">С вами связывается<br />наш менеджер по продажам<br />и обсуждает интересующие<br />вас детали
            </div>
        </div>
        <div class="c_5_2_u">
            <img src="img/нсвсвдвриувн.png" alt="" class="hvr-bounce-out c_7_2_i">
            <div class="c_5_2_c">Наши специалисты встречаются <br />с вами для выбора рекламы <br />и уточнения всех нюансов
            </div>
        </div>
        <div class="c_5_2_u">
            <img src="img/пкиир.png" alt="" class="hvr-bounce-out c_7_2_i">
            <div class="c_5_2_c">Подписание контракта <br />и изготавление рекламы</div>
        </div>
        <div class="c_5_2_u">
            <img src="img/увриспук.png" alt="" class="hvr-bounce-out c_7_2_i">
            <div class="c_5_2_c">Установка вашей <br />рекламы и сопровождение <br />по условиям контракта
            </div>
        </div>
    </div>
</div>

<div class="c_8">
    <div class="c_5_1">Наши контакты</div>
    <div class="c_8_2">
        Республика Казахстан, 050000, город Алматы,<br  />проспект Аль-Фараби 71/21<br  />тел.: +7 /727/ 221 31 13<br  />факс.: +7 /727/ 221 31 13<br  />e-mail: almatyunion@gmail.com<br  /><br  /><br  />www.aunion.kz<br  />
    </div>
    <div class="c_8_3">
        <a href=""><img src="img/f.png" alt="" class="hvr-bob c_8_3_i"></a>
        <a href=""><img src="img/B.png" alt="" class="hvr-bob c_8_3_i"></a>
        <a href=""><img src="img/insta.png" alt="" class="hvr-bob c_8_3_i"></a>
        <a href=""><img src="img/t.png" alt="" class="hvr-bob c_8_3_i"></a>
        <a href=""><img src="img/email.png" alt="" class="hvr-bob c_8_3_i"></a>
    </div>

    <div class="c_1_2" style="    margin: 30px;">
        Оставьте заявку
    </div>
    <div class="c_1_3">
        Мы гарантируем конфиденциальность ваших данных
    </div>
    <div id="contactform2">
        <div class="c_2">
            <div class="c_2_1">
                <div class="c_2_1_i">
                    <img src="img/чел.png" alt="">
                    <input type="text" class="inp_1" name="name" value="" placeholder="Ваше имя" >
                </div>
                <div class="c_2_1_i">
                    <img src="img/пис.png" alt="">
                    <input type="text" class="inp_1" name="email" value="" placeholder="Ваш E-mail" >
                </div>
                <div class="c_2_1_i">
                    <img src="img/тел.png" alt="">
                    <input type="text" class="inp_1" name="phone" value="" placeholder="Ваш номер телефона" >
                </div>
                <img src="img/звонок.png" alt="" class="c_2_i" id="zakaz_1">
            </div>
            <div class="c_2_2">
                Мы свяжемся в ближайшее время
            </div>
        </div>
    </div>
</div>
<div class="c_9">
    <img class="c_9_1" src="img/logogo.png" alt="">
    <div class="c_9_2">ТОО «Union Media» БИН 150440022738<br />Все права защищены. 2015</div>
</div>
</body>
</html>